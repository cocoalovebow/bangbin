# -*- coding: utf-8 -*-

from LineAPI.linepy import *
from LineAPI.linepy import LINE, OEPoll
from LineAPI.akad.ttypes import ChatRoomAnnouncementContents, OpType, MediaType, ContentType, ApplicationType, TalkException, ErrorCode
import multiprocessing
from multiprocessing import Process
from time import sleep
from Naked.toolshed.shell import execute_js
import threading, traceback, newqr, time, random

botStart = time.time()

appJS = "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5"
token = newqr.NewQRLogin().loginWithQrCode("ios_ipad")
if token:
    me = LINE(token, appName=appJS)
else:
    pass
admin = ["u6675618f1c2f6a8b87d2d6be32cbe1bd"]
oepoll = OEPoll(me)

lineMID = me.getProfile().mid
def toChar(text):
    normal = 'abcdefghijklmnopqrstuvwxyz'
    tochange = 'abcdefghijklmnopqrstuvwxyz'
    for i in range(len(normal)):
        text = text.lower().replace(normal[i], tochange[i])
    return text

helpKick = toChar('''คำสั่งของอาบัง (s/+)
- KL↭ลบกลุ่ม
- oo [@, name]↭เตะ
- check↭เช็คบัค
**เฉพาะแอดมินดึงเข้ากลุ่มเตะทันที''')

def clientBot(op):
    global me
    try:
        if op.type == 13:
          if op.param1 in admin:
             if op.param2 :
                try:
                   me.acceptGroupInvitation(op.param1)
                   mem = [c.mid for c in me.getGroup(op.param1).members]
                   targets = []
                   for x in mem:
                      if x not in admin:
                        targets.append(x)
                   if targets:
                       imnoob = 'simple.js gid={} token={} app={}'.format(op.param1, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                       for target in targets:
                          imnoob += ' uid={}'.format(target)
                       success = execute_js(imnoob)
                       if success:
                          me.sendMessage(op.param1, "เตะสำเร็จ %i คน" % len(targets))
                       else:
                          me.sendMessage(op.param1, 'ล้มเหลวเตะ %i คน' % len(targets))
                   else:
                      me.sendMessage(op.param1, 'ไม่พบเป้าหมาย')
                except:
                   me.acceptGroupInvitation(op.param1)
                   me.sendMessage(op.param1,'Limited')
 
        if op.type in [25,26]:
            msg = op.message
            to = msg.to
            makeText = str(msg.text)
            if makeText == None:return
            
            if makeText.lower() == 'mid':
               me.sendMessage(to, msg._from)
            if makeText.lower().startswith("อาบังยกเชิญ"):
                                if msg._from in lineMID:                                
                                    if msg.toType == 2:
                                        group = me.getGroup(msg.to)
                                        gMembMids = [contact.mid for contact in group.invitee]
                                        k = len(gMembMids)//20
                                        me.sendMessage(msg.to,"[ ยกค้างเชิญ จำนวน {} คน] \nรอสักครู่...".format(str(len(gMembMids))))
                                        num=1
                                        for i in range(k+1):
                                            for j in gMembMids[i*20 : (i+1)*20]:
                                                time.sleep(random.uniform(0.5,0.4))
                                                me.cancelGroupInvitation(msg.to,[j])
                                                print ("[Command] "+str(num)+" => "+str(len(gMembMids))+" cancel members")
                                                num = num+1
                                            me.sendMessage(receiver,"รอสักครู่เดียวยกต่อ 40 คน")
                                            time.sleep(random.uniform(15,10))
                                        me.sendMessage(receiver,"เรียบร้อยครับนายท่าน".format(str(len(gMembMids))))
                                        time.sleep(random.uniform(0.95,1))
                                        gname = me.getGroup(receiver).name
                                        me.sendMessage(Notify,"[ ยกค้างเชิญ >> "+gname+"  <<] \n จำนวน {} คน เรียบร้อยแล้ว👏\n『 B O T ของอาบังพาบิน』".format(str(len(gMembMids))))
                                        time.sleep(random.uniform(0.95,1))
                                        me.leaveGroup(receiver)            
            if makeText.lower() in ['คำสั่ง', 'help1']:
                me.sendMessage(msg.to, str(helpKick))
            
            if makeText.lower() == 'KL': 
                mem = [c.mid for c in me.getGroup(to).members]
                targets = []
                for x in mem:
                    if x not in admin:
                       targets.append(x)
                if targets:
                   imnoob = 'simple.js gid={} token={} app={}'.format(to, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                   for target in targets:
                      imnoob += ' uid={}'.format(target)
                   success = execute_js(imnoob)
                   if success:
                      me.sendMessage(to, "เตะสำเร็จ %i คน" % len(targets))
                   else:
                      me.sendMessage(to, 'ล้มเหลวเตะ %i คน' % len(targets))
                else:
                    me.sendMessage(to, 'ไม่พบเป้าหมาย')
                    
            if makeText.lower().startswith("oo"):
               try:
                    sep = makeText.split(" ")
                    midn = makeText.replace(sep[0] + " ","")
                    G = me.getGroup(msg.to)
                    members = [G.mid for G in G.members]
                    targets = []
                    imnoob = 'simple.js gid={} token={} app={}'.format(to, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                    for mids in members:
                       contact = me.getContact(mids)
                       testt = contact.displayName.lower()
                       if midn in testt:
                           targets.append(contact.mid)
                    if targets == []:
                       return me.sendMessage(to, toChar("not found name "+midn)) 
                    for target in targets:
                       imnoob += ' uid={}'.format(target)
                    success = execute_js(imnoob)
               except:pass
               


    except Exception as e:print(e)
                  
def run():
    while True:
        try:
            ops = oepoll.singleTrace(count=50)
            if ops != None:
                for op in ops:
                    threads = []
                    for i in range(1):
                        thread = threading.Thread(target=clientBot(op))
                        threads.append(thread)
                        thread.start()
                        oepoll.setRevision(op.revision)
            for thread in threads:
                thread.join()
        except: pass
if __name__ == "__main__":
    run()
